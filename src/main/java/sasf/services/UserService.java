package sasf.services;

import java.util.List;

import sasf.dto.GenericResponseDto;
import sasf.entities.User;

public interface UserService {

    String message() throws Exception;

    GenericResponseDto<User> createUser(User user) throws Exception;

    GenericResponseDto<User> getUserById(Long id) throws Exception;
    
    GenericResponseDto<List<User>> getAllUsers() throws Exception;
    
    GenericResponseDto<User> updateUser(Long id, User user) throws Exception;
    
    GenericResponseDto<User> deleteUser(Long id) throws Exception;
    
}
