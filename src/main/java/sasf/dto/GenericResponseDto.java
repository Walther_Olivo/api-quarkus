package sasf.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GenericResponseDto<T> {
    private String status;
    private int code;
    private String meesage;
    private T payload;
}
