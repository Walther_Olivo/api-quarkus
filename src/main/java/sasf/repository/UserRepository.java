package sasf.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import sasf.entities.User;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {
    
}
