package sasf.controller;

import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import sasf.dto.GenericResponseDto;
import sasf.entities.User;
import sasf.servicesImpl.UserServiceImpl;

@Path("user")
public class UserController {
    
    @Inject
    private UserServiceImpl userService;

    @Path("message")
    @GET
    public String message() throws Exception{
        return userService.message();
    }

    @POST
    public Response createUser(User user) {
        try {
            GenericResponseDto<User> response = userService.createUser(user);
            return Response.status(response.getCode()).entity(response).build();
        } catch (Exception e) {
            GenericResponseDto<User> response =  new GenericResponseDto<>("error", 400 , "Error: " + e.getMessage(), null);
            return Response.status(response.getCode()).entity(response).build();                
        }
    }

    @PUT
    @Path("/{id}")
    public Response updateUser(@PathParam("id") Long id, User user) {
        try {
            GenericResponseDto<User> response = userService.updateUser(id, user);
            return Response.status(response.getCode()).entity(response).build();
        } catch (Exception e) {
            GenericResponseDto<User> response =  new GenericResponseDto<>("error", 400 , "Error: " + e.getMessage(), null);
            return Response.status(response.getCode()).entity(response).build();                
        }
    }

    @GET
    @Path("/{id}")
    public Response getUserById(@PathParam("id") Long id) {
        try {
            GenericResponseDto<User> response = userService.getUserById(id);
            return Response.status(response.getCode()).entity(response).build();
        } catch (Exception e) {
            GenericResponseDto<User> response =  new GenericResponseDto<>("error", 400 , "Error: " + e.getMessage(), null);
            return Response.status(response.getCode()).entity(response).build();                
        }
    }


    @GET
    public Response getAllUsers() {
        try {
            GenericResponseDto<List<User>> response = userService.getAllUsers();
            return Response.status(response.getCode()).entity(response).build();
        } catch (Exception e) {
            GenericResponseDto<User> response =  new GenericResponseDto<>("error", 400 , "Error: " + e.getMessage(), null);
            return Response.status(response.getCode()).entity(response).build();                
        }
    }

    @DELETE
    @Path("/{id}")
    public Response deleteUser(@PathParam("id") Long id) {
        try {
            GenericResponseDto<User> response = userService.deleteUser(id);
            return Response.status(response.getCode()).entity(response).build();
        } catch (Exception e) {
            GenericResponseDto<User> response =  new GenericResponseDto<>("error", 400 , "Error: " + e.getMessage(), null);
            return Response.status(response.getCode()).entity(response).build();                
        }
    }

}
