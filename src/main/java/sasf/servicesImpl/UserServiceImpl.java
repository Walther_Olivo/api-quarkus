package sasf.servicesImpl;

import sasf.dto.GenericResponseDto;
import sasf.entities.User;
import sasf.services.UserService;

import java.util.List;
import java.util.Optional;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
 

@ApplicationScoped
public class UserServiceImpl implements UserService {

    @Inject
    EntityManager entityManager;
  
    @Override
    public String message() throws Exception {
        return "{\"message\":\"Hello, World!\"}";
      }

    @Override
    @Transactional
    public GenericResponseDto<User> createUser(User user) throws Exception {
         entityManager.persist(user);
         return new GenericResponseDto<>("succes", 201 , "User created successfull", user);
    }

    @Override
    public GenericResponseDto<User> getUserById(Long id) throws Exception {
        Optional<User> user = Optional.ofNullable(entityManager.find(User.class, id));
        if (user.isPresent()){
            return new GenericResponseDto<>("succes", 200, "User found successfull", user.get());
        }else{
            return new GenericResponseDto<>("error", 404 , "User with id not found: " + id, null);
        }
    }

    @Override
    public GenericResponseDto<List<User>> getAllUsers() throws Exception {
        List<User> users = entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
        return new GenericResponseDto<>("succes", 201 , "User founds successfull", users);
    }

    @Override
    @Transactional
    public GenericResponseDto<User> updateUser(Long id, User user) throws Exception {
        User existingUser = getUserById(id).getPayload();
        if( existingUser !=null){
            existingUser.setName(user.getName());
            existingUser.setLastName(user.getLastName());
            existingUser.setAge(user.getAge());
            existingUser.setAddress(user.getAddress());
            entityManager.merge(existingUser);
            return new GenericResponseDto<>("succes", 201 , "User updated successfull", existingUser);
        }else{
            return new GenericResponseDto<>("error", 404 , "User with id not found: " + id, null);
        }
    }

    @Override
    @Transactional
    public GenericResponseDto<User> deleteUser(Long id) throws Exception {
        User existingUser = getUserById(id).getPayload();
        if( existingUser !=null){
            entityManager.remove(existingUser);
            return new GenericResponseDto<>("succes", 201 , "User delete successfull", existingUser);
        }else{
            return new GenericResponseDto<>("error", 404 , "User with id not found: " + id, null);
        }
        }
}
